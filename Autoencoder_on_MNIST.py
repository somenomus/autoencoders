import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

#load mnist data

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

#the one_hot = true parameters ensures that all the features are one hot encoded. One hot
#encoding is a technique by which categorial variables are converted into a form that could be feed
#into a ML algorithms

#configuration

learning_rate = 0.001
training_epochs= 50
batch_size= 64
display_step = 1
examples_to_show = 10

#size of image,28x28 each image, so,

n_input = 784

#size of hidden features

n_hidden_1 = 256
n_hidden_2 = 128

#placeholder for input

X = tf.placeholder("float", [None, n_input])  #None= tensor may hold an arbitary num of imgs

# Weighta and bias using random normal

weights = {'encoder_h1':tf.Variable(tf.random_normal([n_input, n_hidden_1])),
           'encoder_h2':tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
           'decoder_h1':tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1])),
           'decoder_h2':tf.Variable(tf.random_normal([n_hidden_1, n_input]))}

biases = {'encoder_b1':tf.Variable(tf.random_normal([n_hidden_1])),
           'encoder_b2':tf.Variable(tf.random_normal([n_hidden_2])),
           'decoder_b1':tf.Variable(tf.random_normal([n_hidden_1])),
           'decoder_b2':tf.Variable(tf.random_normal([n_input]))}

#we split the network into 2 complementary fully connected networks.an encoder, an decoder.

encoder_in = tf.nn.sigmoid(tf.add(tf.matmul(X, weights['encoder_h1']), biases['encoder_b1']))

'''
the input data encoding is just a simple matrix multiplacation.  input data with dimention 784
is reduced to lower dimention 256 using matrix multiplication (W*x + b) = encoder_in
W=weight b=biasencoder_h1 & b1 in thisw case.
'''
#2nd step of encoding, data compression . and output of encoder

encoder_out = tf.nn.sigmoid(tf.add(tf.matmul(encoder_in,weights['encoder_h2']), biases['encoder_b2']))

'''
the input in this case was 256 and compressed to lower to 128 using same above method. we used sigmoid
as an activation function.
 
now decoder is oposite.
'''

decoder_in = tf.nn.sigmoid(tf.add(tf.matmul(encoder_out, weights['decoder_h1']), biases['decoder_b1']))

'''
in formula it means (W*encoder_out+b)=decoder_in . decoder_h1 of size 256*128 and decoder_b1 of size
256. final task is to represent 784, original size. by:
'''
decoder_out= tf.nn.sigmoid(tf.add(tf.matmul(decoder_in, weights['decoder_h2']), biases['decoder_b2']))


#set parameter
y_pred = decoder_out

#the network will learn weather the input data X is equal to the decoded data or not. so we define

y_true = X

'''
the point of autoencoder is to reducew matrix and also the cost. so, cost function, mean sqr error
of y true and y pred. so,
'''
cost = tf.reduce_mean(tf.pow(y_true - y_pred, 2))

'''
tf.pow(x, y, name=None) Computes the power of one value to another.

Given a tensor x and a tensor y, this operation computes \(x^y\) for corresponding elements 
in x and y. tf.pow(2.0, 1.5) = 2.8284271, (2^1.5)

To optimize cost function , we will use RMSprop
'''

optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

#run

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    total_batch = int(mnist.train.num_examples/batch_size)   #calculate the total batch number if we have batch_size of size
    #start training
    for epoch in range(training_epochs):
        for i in range(total_batch):
            batch_xs, batch_ys = mnist.train.next_batch(batch_size)
            #run the optimization procedure. feeding thew execution graph
            _, c = sess.run([optimizer,cost], feed_dict= {X:batch_xs})
        
        #display the result
        
        if epoch%display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "Cost= ", "{:.9f}".format(c))
            
    print("optimization finished")
            
        #test rthre model.
        
    encode_decode = sess.run(y_pred, feed_dict={X: mnist.test.images[:examples_to_show]})
        
        #compare the original img with reconstructed one.
        
    f, a = plt.subplots(2, 10, figsize=(10, 2))
    for i in range(examples_to_show):
        a[0][i].imshow(np.reshape(mnist.test.images[i], (28, 28)))
        a[1][i].imshow(np.reshape(encode_decode[i], (28, 28)))
    f.show()
    plt.draw()
    plt.show()

        








